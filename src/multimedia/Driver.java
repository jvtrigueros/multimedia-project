package multimedia;


import magick.MagickException;
import multimedia.model.LZW;
import multimedia.model.ModifiedImage;
import multimedia.util.ProjectConstants;

/**
 * 
 */

/**
 * @author trigueros
 * O
 */
public class Driver
{

    /**
     * @param args
     * @throws MagickException
     */
    public static void main( String[] args ) throws MagickException
    {
        ModifiedImage model = new ModifiedImage(ProjectConstants.IMAGE_DIR + "testImage.png");
//        ModifiedImage distorted = new ModifiedImage("/home/trigueros/workspace/multimedia-project/imagemagick_out/0024Encoded.RGB.png");

        LZW lzw = new LZW(model.getImagePixelHolder().toString());
        
//        model.getImagePixelHolder().setRgbVector(ColorspaceUtil.ConvertToYIQ(model.getImagePixelHolder().getRgbVector()));
        model.writeToBinaryFile("testImageLZW.RGB", lzw.encode());
        
    }
}