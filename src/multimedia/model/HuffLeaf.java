package multimedia.model;

class HuffLeaf extends HuffTree{
	    
	    private int value;
	    
	    public HuffLeaf(int value, int frequency)
	    {
	      this.value = value;
	      this.frequency = frequency;//frequency coming from HuffTree
	    }
	    
	    public int getValue(){
	      return value;
	    }
	  
	  }