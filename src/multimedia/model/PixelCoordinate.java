package multimedia.model;

import java.util.Scanner;
import java.util.Vector;

public class PixelCoordinate
{
    private Vector<ColorspaceInterface> rgbVector;
    private int width;

    public PixelCoordinate()
    {
        rgbVector = new Vector<ColorspaceInterface>();
        width = 0;
    }
    
    public PixelCoordinate(byte[] pixels, int width)
    {
        rgbVector = ColorspaceUtil.CreateInitialRGBVector(pixels);
        this.width = width;
    }
    
    public PixelCoordinate(Vector<ColorspaceInterface> rgbVector, int width)
    {
        this.rgbVector = rgbVector;
        this.width = width;
    }

    public ColorspaceInterface getPixelAt( int x, int y )
    {
        return rgbVector.get(y * width + x);
    }
    
    public String flattenVector()
    {
        StringBuilder flatVector = new StringBuilder();
        
        for ( ColorspaceInterface pixel : rgbVector )
        {
            flatVector.append(pixel.getFirstComponent());
            flatVector.append(' ');
            flatVector.append(pixel.getSecondComponent());
            flatVector.append(' ');
            flatVector.append(pixel.getThirdComponent());
            flatVector.append(' ');
        }
        
        return flattenVector().toString();
    }
    
    public Vector<ColorspaceInterface> getRgbVector()
    {
        return rgbVector;
    }
    
    public void setRgbVector( Vector<ColorspaceInterface> rgbVector )
    {
        this.rgbVector = rgbVector;
    }

    public String[] createComponentStrings()
    {
        StringBuilder first = new  StringBuilder();
        StringBuilder second = new StringBuilder();
        StringBuilder third = new  StringBuilder();
        
        for ( ColorspaceInterface pixel : rgbVector )
        {
            first.append(pixel.getFirstComponent());
            first.append(' ');
            second.append(pixel.getSecondComponent());
            second.append(' ');
            third.append(pixel.getThirdComponent());
            third.append(' ');
        }
        
        first.deleteCharAt(first.length() - 1);
        second.deleteCharAt(second.length() - 1);
        third.deleteCharAt(third.length() - 1);

        
        return (new String[]{first.toString(), second.toString(), third.toString() });
    }
    
    public static String mergeComponentStrings ( String[] components )
    {
        StringBuilder mergedComponents = new StringBuilder();
        Scanner[] componentIterator = new Scanner[3];
        for ( int i = 0; i < components.length; i++ )
        {
            componentIterator[i] = new Scanner(components[i]);
        }
        
        while( componentIterator[0].hasNext() )
        {
            mergedComponents.append(componentIterator[0].next() + " ");
            mergedComponents.append(componentIterator[1].next() + " ");
            mergedComponents.append(componentIterator[2].next() + " ");
        }
        
        // remove trailing space
        return (mergedComponents.toString()).trim();
    }
    
    @Override
    public String toString()
    {
        StringBuilder first = new  StringBuilder();
        
        for ( ColorspaceInterface pixel : rgbVector )
        {
            first.append(pixel.getFirstComponent());
            first.append(' ');
            first.append(pixel.getSecondComponent());
            first.append(' ');
            first.append(pixel.getThirdComponent());
            first.append(' ');
        }
        
        // Remove the final un-needed space.
        first.deleteCharAt(first.length() - 1);
                
        return first.toString();
    }
}
