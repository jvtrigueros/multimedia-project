package multimedia.model;

class HuffNode extends HuffTree{
  
    private HuffTree left;
    private HuffTree right;
  
    public HuffNode(int frequency, HuffTree left, HuffTree right)
    {
      this.frequency = frequency;//from HuffTree is where frequency is coming
      this.left = left;
      this.right = right;
    }
  
    public HuffTree getRight()
    {
      return right;
    }
    
    public HuffTree getLeft()
    {
      return left;
    }
  }