package multimedia.model;

/**
 * 
 * @author ricardo
 *
 */

	public class YCbCr implements ColorspaceInterface
	{
	    public float y = 0.0f;
	    public float cb = 0.0f;
	    public float cr = 0.0f;
	    
	    public YCbCr() 
	    {
	        // TODO Auto-generated constructor stub
	    }
	    
	    public YCbCr( float y, float cb, float cr)
	    {
	        this.y = y;
	        this.cb = cb;
	        this.cr = cr;
	    }
	    
	    @Override
	    public void setFirstComponent( float firstComponent )
	    {
	        y = firstComponent;
	    }

	    @Override
	    public void setSecondComponent( float secondComponent )
	    {
	        cb = secondComponent;
	    }

	    @Override
	    public void setThirdComponent( float thirdComponent )
	    {
	        cr = thirdComponent;
	    }

	    @Override
	    public float getFirstComponent()
	    {
	        return y;
	    }

	    @Override
	    public float getSecondComponent()
	    {
	        return cb;
	    }

	    @Override
	    public float getThirdComponent()
	    {
	        return cr;
	    }

	    @Override
	    public String toString()
	    {
	        return "Y: " + y + "\nCb: " + cb + "\nCr: " + cr;
	    }
	}
