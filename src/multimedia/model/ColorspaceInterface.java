/**
 * 
 */
package multimedia.model;

/**
 * This is an interface that is used to model how all the other colorspaces should look like.
 * 
 * @author trigueros
 *
 */
public interface ColorspaceInterface
{
    /**
     * @param firstComponent the firstComponent to set
     */
    public void setFirstComponent( float firstComponent );
    
    /**
     * @param secondComponent the secondComponent to set
     */
    public void setSecondComponent( float secondComponent );

    /**
     * @param thirdComponent the thirdComponent to set
     */
    public void setThirdComponent( float thirdComponent );
    
    /**
     * Get the first color component.
     * @return value of first component.
     */
    public float getFirstComponent();
    
    /**
     * Get the second color component.
     * @return value of second component.
     */
    public float getSecondComponent();
    
    /**
     * Get the third color component.
     * @return value of third component.
     */
    public float getThirdComponent();

    public String toString();
}
