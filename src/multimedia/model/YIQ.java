package multimedia.model;

/**
 * 
 * @author ricardo
 *
 */

	public class YIQ implements ColorspaceInterface
	{
	    public float y = 0.0f;
	    public float i = 0.0f;
	    public float q = 0.0f;
	    
	    public YIQ() 
	    {
	        // TODO Auto-generated constructor stub
	    }
	    
	    public YIQ( float y, float i, float q)
	    {
	        this.y = y;
	        this.i = i;
	        this.q = q;
	    }
	    
	    @Override
	    public void setFirstComponent( float firstComponent )
	    {
	        y = firstComponent;
	    }

	    @Override
	    public void setSecondComponent( float secondComponent )
	    {
	        i = secondComponent;
	    }

	    @Override
	    public void setThirdComponent( float thirdComponent )
	    {
	        q = thirdComponent;
	    }

	    @Override
	    public float getFirstComponent()
	    {
	        return y;
	    }

	    @Override
	    public float getSecondComponent()
	    {
	        return i;
	    }

	    @Override
	    public float getThirdComponent()
	    {
	        return q;
	    }

	    @Override
	    public String toString()
	    {
	        return "Y: " + y + "\nI: " + i + "\nQ: " + q;
	    }
	}
