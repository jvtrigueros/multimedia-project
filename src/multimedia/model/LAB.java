/**
 * 
 */
package multimedia.model;

/**
 * @author trigueros
 *
 */
public class LAB implements ColorspaceInterface
{

    public float L;
    public float a;
    public float b;
    
    public LAB() {}
    
    public LAB( float L, float a, float b )
    {
        this.L = L;
        this.a = a;
        this.b = b;
    }
    
    /* (non-Javadoc)
     * @see multimedia.model.ColorspaceInterface#setFirstComponent(float)
     */
    @Override
    public void setFirstComponent( float firstComponent )
    {
        L = firstComponent;
    }

    /* (non-Javadoc)
     * @see multimedia.model.ColorspaceInterface#setSecondComponent(float)
     */
    @Override
    public void setSecondComponent( float secondComponent )
    {
        a = secondComponent;
    }

    /* (non-Javadoc)
     * @see multimedia.model.ColorspaceInterface#setThirdComponent(float)
     */
    @Override
    public void setThirdComponent( float thirdComponent )
    {
        b = thirdComponent;
    }

    /* (non-Javadoc)
     * @see multimedia.model.ColorspaceInterface#getFirstComponent()
     */
    @Override
    public float getFirstComponent()
    {
        return L;
    }

    /* (non-Javadoc)
     * @see multimedia.model.ColorspaceInterface#getSecondComponent()
     */
    @Override
    public float getSecondComponent()
    {
        return a;
    }

    /* (non-Javadoc)
     * @see multimedia.model.ColorspaceInterface#getThirdComponent()
     */
    @Override
    public float getThirdComponent()
    {
        return b;
    }

    @Override
    public String toString()
    {
        return "L: " + L + "\na: " + a + "\nb: " + b;
    }
}
