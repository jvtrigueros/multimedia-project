package multimedia.model;

public class DefaultColorspace implements ColorspaceInterface
{
    float firstComponent;
    float secondComponent;
    float thirdComponent;
    
    public DefaultColorspace()
    {
        firstComponent = 0.0f;
        secondComponent = 0.0f;
        thirdComponent = 0.0f;
    }

    @Override
    public void setFirstComponent( float firstComponent )
    {
        this.firstComponent = firstComponent;
    }
    
    
    @Override
    public void setSecondComponent( float secondComponent )
    {
        this.secondComponent = secondComponent;
    }
    
    
    @Override
    public void setThirdComponent( float thirdComponent )
    {
        this.thirdComponent = thirdComponent;
    }

    @Override
    public float getFirstComponent()
    {
        return firstComponent;
    }

    @Override
    public float getSecondComponent()
    {
        return secondComponent;
    }

    @Override
    public float getThirdComponent()
    {
        return thirdComponent;
    }

    @Override
    public String toString()
    {
        return firstComponent + "\n" + secondComponent + "\n" + thirdComponent;
    }

}
