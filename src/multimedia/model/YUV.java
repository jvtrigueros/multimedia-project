package multimedia.model;

/**
 * 
 * @author ricardo
 *
 */

	public class YUV implements ColorspaceInterface
	{
	    public float y = 0.0f;
	    public float u = 0.0f;
	    public float v = 0.0f;
	    
	    public YUV() 
	    {
	        // TODO Auto-generated constructor stub
	    }
	    
	    public YUV( float y, float u, float v)
	    {
	        this.y = y;
	        this.u = u;
	        this.v = v;
	    }
	    
	    @Override
	    public void setFirstComponent( float firstComponent )
	    {
	        y = firstComponent;
	    }

	    @Override
	    public void setSecondComponent( float secondComponent )
	    {
	        u = secondComponent;
	    }

	    @Override
	    public void setThirdComponent( float thirdComponent )
	    {
	        v = thirdComponent;
	    }

	    @Override
	    public float getFirstComponent()
	    {
	        return y;
	    }

	    @Override
	    public float getSecondComponent()
	    {
	        return u;
	    }

	    @Override
	    public float getThirdComponent()
	    {
	        return v;
	    }

	    @Override
	    public String toString()
	    {
	        return "Y: " + y + "\nU: " + u + "\nV: " + v;
	    }
	}
