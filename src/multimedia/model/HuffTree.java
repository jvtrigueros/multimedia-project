package multimedia.model;

@SuppressWarnings("rawtypes")
abstract class HuffTree implements Comparable
{

  int frequency;
  
  public int getFrequency()
  {
    return frequency;
  }
  
  public int compareTo(Object object)
  {
    HuffTree tree = (HuffTree)object;
    if (frequency == tree.frequency)//if the objects 
    			//have the same frequency value, it returns a tiebraker
    	        //based on the value of the relative hashcode values
    {
      return (hashCode() - tree.hashCode());
    }
    
    else//returns negative or positive value depending if frequency is
    	//greater or smaller than the freq of the object in parameter
    {
      return frequency - tree.frequency;
    }
  }
}