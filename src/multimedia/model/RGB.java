/**
 * 
 */
package multimedia.model;

/**
 * A container for RGB colors.
 * 
 * @author trigueros
 * 
 */
public class RGB implements ColorspaceInterface
{
    public float r = 0.0f;
    public float g = 0.0f;
    public float b = 0.0f;

    public RGB()
    {
    }

    public RGB(int r, int g, int b)
    {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public RGB(ColorspaceInterface colorPixel)
    {
        r = colorPixel.getFirstComponent();
        g = colorPixel.getSecondComponent();
        b = colorPixel.getThirdComponent();
    }

    public RGB add( ColorspaceInterface pixel )
    {
        RGB rgb = new RGB();

        rgb.r = this.r + pixel.getFirstComponent();
        rgb.g = this.g + pixel.getSecondComponent();
        rgb.b = this.b + pixel.getThirdComponent();

        return rgb;
    }

    public RGB subtract( ColorspaceInterface pixel )
    {
        RGB rgb = new RGB();

        rgb.r = this.r - pixel.getFirstComponent();
        rgb.g = this.g - pixel.getSecondComponent();
        rgb.b = this.b - pixel.getThirdComponent();

        return rgb;
    }

    public RGB divideBy( float divisor )
    {
        RGB rgb = new RGB();

        rgb.r = this.r / divisor;
        rgb.g = this.g / divisor;
        rgb.b = this.b / divisor;

        return rgb;
    }

    public RGB multBy( float factor )
    {
        RGB rgb = new RGB();

        rgb.r = this.r * factor;
        rgb.g = this.g * factor;
        rgb.b = this.b * factor;
        
        return rgb;
    }

    public void square()
    {
        r *= r;
        g *= g;
        b *= b;
    }
    
    @Override
    public void setFirstComponent( float firstComponent )
    {
        r = firstComponent;
    }

    @Override
    public void setSecondComponent( float secondComponent )
    {
        g = secondComponent;
    }

    @Override
    public void setThirdComponent( float thirdComponent )
    {
        b = thirdComponent;
    }

    @Override
    public float getFirstComponent()
    {
        return r;
    }

    @Override
    public float getSecondComponent()
    {
        return g;
    }

    @Override
    public float getThirdComponent()
    {
        return b;
    }

    @Override
    public String toString()
    {
        return "Red: " + r + "\nGreen: " + g + "\nBlue: " + b;
    }
}
