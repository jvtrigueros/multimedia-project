package multimedia.model;

/**
 * 
 * @author ricardo
 *
 */

	public class HSL implements ColorspaceInterface
	{
	    public float h = 0.0f;
	    public float s = 0.0f;
	    public float l = 0.0f;
	    
	    public HSL() 
	    {
	        // TODO Auto-generated constructor stub
	    }
	    
	    public HSL( float h, float s, float l)
	    {
	        this.h = h;
	        this.s = s;
	        this.l = l;
	    }
	    
	    @Override
	    public void setFirstComponent( float firstComponent )
	    {
	        h = firstComponent;
	    }

	    @Override
	    public void setSecondComponent( float secondComponent )
	    {
	        s = secondComponent;
	    }

	    @Override
	    public void setThirdComponent( float thirdComponent )
	    {
	        l = thirdComponent;
	    }

	    @Override
	    public float getFirstComponent()
	    {
	        return h;
	    }

	    @Override
	    public float getSecondComponent()
	    {
	        return s;
	    }

	    @Override
	    public float getThirdComponent()
	    {
	        return l;
	    }

	    @Override
	    public String toString()
	    {
	        return "H: " + h + "\nS: " + s + "\nL: " + l;
	    }
	}
