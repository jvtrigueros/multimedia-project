/**
 * 
 */
package multimedia.model;


/**
 * A container for XYZ colors.
 * 
 * @author trigueros
 *
 */
public class XYZ implements ColorspaceInterface
{
    public float x = 0.0f;
    public float y = 0.0f;
    public float z = 0.0f;
    
    public XYZ() 
    {
        // TODO Auto-generated constructor stub
    }
    
    public XYZ( float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    @Override
    public void setFirstComponent( float firstComponent )
    {
        x = firstComponent;
    }

    @Override
    public void setSecondComponent( float secondComponent )
    {
        y = secondComponent;
    }

    @Override
    public void setThirdComponent( float thirdComponent )
    {
        z = thirdComponent;
    }

    @Override
    public float getFirstComponent()
    {
        return x;
    }

    @Override
    public float getSecondComponent()
    {
        return y;
    }

    @Override
    public float getThirdComponent()
    {
        return z;
    }

    @Override
    public String toString()
    {
        return "X: " + x + "\nY: " + y + "\nZ: " + z;
    }
}
