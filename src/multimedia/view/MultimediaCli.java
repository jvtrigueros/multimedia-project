/**
 * 
 */
package multimedia.view;

import java.awt.Point;
import java.io.File;
import java.io.FileFilter;
import java.util.Scanner;

import magick.MagickException;
import multimedia.model.ModifiedImage;
import multimedia.util.ProjectConstants;

/**
 * @author trigueros
 *
 */
public class MultimediaCli
{

    /**
     * @param args
     */
    public static void main( String[] args )
    {
        Scanner userInput = new Scanner(System.in);
        int userCommand = -1;
        Point[] pixCoords = null;
        boolean isRunning = true;
        String[] filenames = initializeFilenames();
        ModifiedImage magickHandle = null;
        
        // Initial Run
        printFilenames(filenames);
        prompt();
        userCommand = userInput.nextInt();
        String currentFilename = filenames[userCommand-1];
        try
        {
            magickHandle = new ModifiedImage(ProjectConstants.IMAGE_DIR + currentFilename);
        } catch ( MagickException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        // Entering loop
        while( isRunning )
        {
            printMenu();
            prompt();
            userCommand = userInput.nextInt();
            userInput.nextLine();
            try
            {
                switch ( userCommand )
                {
                    case 1:
                        printFilenames(filenames);
                        prompt();
                        userCommand = userInput.nextInt();
                        userInput.nextLine();
                        currentFilename = filenames[userCommand-1];
                        magickHandle = new ModifiedImage(ProjectConstants.IMAGE_DIR + currentFilename);
                        break;
                        
                    case 2:
                        pixCoords = selectRegion(userInput);
                        if( magickHandle != null )
                            magickHandle.selectRegion(pixCoords[0], pixCoords[1]);
                        break;
                        
                    case 3:
                        System.out.println(magickHandle.computeRegionColorspacesAverage());
                        break;
                        
                    case 4:
                        System.out.print("Enter percentage: ");
                        userCommand = userInput.nextInt();
                        userInput.nextLine();
                        magickHandle.changeBrightness(userCommand);
                        magickHandle.writeToFile("brightness_" + currentFilename );
                        break;
                        
                    case 5:
                        System.out.print("Enter percentage: ");
                        userCommand = userInput.nextInt();
                        userInput.nextLine();
                        magickHandle.changeSaturation(userCommand);
                        magickHandle.writeToFile("saturation_" + currentFilename );
                        break;
                        
                    case 6:
                        System.out.print("Enter delta change: ");
                        userCommand = userInput.nextInt();
                        userInput.nextLine();
                        magickHandle.changeColorspacesByDelta(userCommand);
                        break;
                        
                    case 7:
                        magickHandle.decreaseBitResolution();
                        break;
                        
                    case 0:
                        isRunning = false;
                        break;
                        
                    default:
                        System.out.println("Invalid Number, try again.");
                        break;
                }
            } catch ( MagickException e )
            {
                e.printStackTrace();
            }
        }
        
        
    }

    private static Point[] selectRegion(Scanner userInput)
    {
        
        String[] coord1, coord2;
        System.out.println("Please enter the coordinates of the region you would like to select. ex. 10,10 ");
        
        System.out.print("First Coordinate: ");
        coord1 = userInput.nextLine().trim().replaceAll(" ", "").split(",");
        
        System.out.print("Second Coordinate: ");
        coord2 = userInput.nextLine().trim().replaceAll(" ", "").split(",");
        
        
        return new Point[]{new Point( Integer.parseInt(coord1[0]), Integer.parseInt(coord1[1]) ), 
                new Point( Integer.parseInt(coord2[0]), Integer.parseInt(coord2[1]) )   };
    }
    
    private static void printMenu()
    {
        String output = 
                "1: Select a new picture.\n" +
                "2: Select new coordinates \n" +
                "3: Print average color values for the selected region. \n" +
                "4: Change brightness by percentage. \n" +
                "5: Change saturation by percentage. \n" +
                "6: Increase every color component by delta and make 7 copies of the image. \n" +
                "7: Decrease the bit resolution of the third color component and make 7 copies of the image. \n" +
                "0: Exit \n";
        System.out.println(output);
    }

    private static void prompt()
    {
        System.out.print("> ");
    }

    private static void printFilenames( String[] filenames )
    {
        System.out.println("Please enter the number for the picture you would like to use: ");
        for( int i = 0; i < filenames.length; i++ )
        {
            System.out.println( "["+ (i+1) + "] " + filenames[i]);
        }
    }

    private static String[] initializeFilenames()
    {
        // Obtain files from the directory
        File files[] = (new File(ProjectConstants.IMAGE_DIR)).listFiles(new FileFilter() {
            
            @Override
            public boolean accept( File pathname )
            {
                if( pathname.getName().endsWith(".png") || pathname.getName().endsWith(".jpg"))
                    return true;
                return false;
            }
        });
        
        // Get the filenames and store them
        String[] availablePictures = new String[files.length];
        for ( int i = 0; i < availablePictures.length; i++ )
        {
            availablePictures[i] = files[i].getName();
        }
        
        return availablePictures;
    }

}
