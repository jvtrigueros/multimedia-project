package multimedia.util;


public class ProjectConstants
{
    public final static String IMAGE_DIR = System.getProperty("user.dir") + "/" + "imagemagick_tests/";
    public final static String IMAGE_OUTDIR = System.getProperty("user.dir") + "/" + "imagemagick_out/";
    public final static String BINARY_ENCODED = System.getProperty("user.dir") + "/" + "binary_encoded/";
    public final static String BINARY_DECODED = System.getProperty("user.dir") + "/" + "binary_decoded/";
    public final static String TEST_DIR = System.getProperty("user.dir") + "/";
    
    public final static float[] YIQ_MIN_RANGES = new float[]{0.0f, -151.98f, -133.635f};
    public final static float[] YIQ_MAX_RANGES = new float[]{255.0f, 151.98f, 133.635f};
    public final static float[] RGB_MIN_RANGES = new float[]{0f,0f,0f};
    public final static float[] RGB_MAX_RANGES = new float[]{255.0f,255.0f,255.0f};
}
