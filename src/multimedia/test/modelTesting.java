package multimedia.test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Vector;

import magick.ColorspaceType;
import magick.MagickException;
import multimedia.model.ColorspaceInterface;
import multimedia.model.ColorspaceUtil;
import multimedia.model.LZW;
import multimedia.model.ModifiedImage;
import multimedia.model.Predictor;
import multimedia.model.Quantizer;
import multimedia.model.RGB;
import multimedia.util.ProjectConstants;

import org.junit.Before;
import org.junit.Test;

public class modelTesting
{
    private ModifiedImage model;
    private RGB testPixel;
    private String imagePath = ProjectConstants.IMAGE_DIR + "testImage.png";
    private final static double DELTA = 0.1;
    
    @Before
    public void setUp()
    {
        try
        {
            model = new ModifiedImage(imagePath);
            testPixel = new RGB(100,100,100);
        } catch ( MagickException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Tests to see if the conversion from RGB to YIQ went through.
     */
    @Test
    public void convertToYIQ()
    {
        try
        {
            ModifiedImage expectedModel = new ModifiedImage( new File("imagemagick_expected/testImage.YIQ"));
            Vector<ColorspaceInterface> modelVector = ColorspaceUtil.ConvertToYIQ( model.getImagePixelHolder().getRgbVector() );
            Vector<ColorspaceInterface> expectedVector = expectedModel.getImagePixelHolder().getRgbVector();            
            
            assertEqualVector(expectedVector, modelVector);
            
            
        } catch ( FileNotFoundException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    /**
     * Test if quantization with parameters 10,10,10 is working.    
     */
    @Test
    public void quantization()
    {
        try
        {
            ModifiedImage expectedModel = new ModifiedImage( new File("imagemagick_expected/testImageQuantized.RGB"));
            Quantizer quantizer = new Quantizer(model, ColorspaceType.RGBColorspace);
            quantizer.uniformQuantization(10, 10, 10);
            
            assertEqualVector( expectedModel.getImagePixelHolder().getRgbVector(), model.getImagePixelHolder().getRgbVector() );
            
        } catch ( FileNotFoundException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    /**
     * Tests predictive coding A, B, and C.
     */
    @Test
    public void predictiveCodingABC()
    {
        Predictor predictor = new Predictor();
        
        RGB expected = new RGB(95, 95, 95);
        
        ColorspaceInterface actual = predictor.P1(testPixel, new RGB(5,5,5));
        
        assertEquals("First Component", expected.r, actual.getFirstComponent(), DELTA);
        assertEquals("Second Component", expected.g, actual.getSecondComponent(), DELTA);
        assertEquals("Third Component", expected.b, actual.getThirdComponent(), DELTA);
    }

    /**
     * Testing the LZW compression algorithm
     */
    @Test
    public void testLZW()
    {
        try
        {
            Scanner expected = new Scanner(new File("imagemagick_expected/testImageLZW.RGB"));
            LZW lzw = new LZW(model.getImagePixelHolder().toString());
            Scanner actual = new Scanner( lzw.encode() );
            
            while( expected.hasNext() )
            {
                assertEquals(expected.next(), actual.next());
            }
        } catch ( FileNotFoundException e )
        {
            e.printStackTrace();
        }
    }
    
    /**
     * A method that compares two RGB Vectors and decides if they are the same. It uses JUnit calls to make sure that it tells me where it failed.
     * @param expected
     * @param actual
     */
    private void assertEqualVector( Vector<ColorspaceInterface> expected, Vector<ColorspaceInterface> actual )
    {
        for( int i = 0; i < expected.size(); i++ )
        {
            assertEquals("First Component Index: " + i,  expected.elementAt(i).getFirstComponent(),  actual.elementAt(i).getFirstComponent() , DELTA);
            assertEquals("Second Component Index: " + i, expected.elementAt(i).getSecondComponent(), actual.elementAt(i).getSecondComponent(), DELTA);
            assertEquals("Third Component Index: " + i,  expected.elementAt(i).getThirdComponent(),  actual.elementAt(i).getThirdComponent() , DELTA);
        }
    }

}
