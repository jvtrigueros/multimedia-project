package oldCode;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;



import java.awt.GridLayout;

public class MultimediaGui
{

    private JFrame frmMultimediaGraphicalInterface;
    private JTextField imagePathField;
    private ActionButtonListener buttonListener;
    private MousePanelListener mouseListener;
    private JLabel xValueLabel;
    private JLabel yValueLabel;

    /**
     * Launch the application.
     */
    public static void main( String[] args )
    {
        EventQueue.invokeLater(new Runnable() {
            public void run()
            {
                try
                {
                    MultimediaGui window = new MultimediaGui();
                    window.frmMultimediaGraphicalInterface.setVisible(true);
                } catch ( Exception e )
                {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public MultimediaGui()
    {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize()
    {
        frmMultimediaGraphicalInterface = new JFrame();
        frmMultimediaGraphicalInterface.setResizable(false);
        frmMultimediaGraphicalInterface.setTitle("Multimedia Graphical Interface");
        frmMultimediaGraphicalInterface.setBounds(100, 100, 564, 561);
        frmMultimediaGraphicalInterface.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JPanel imagePanel = new JPanel();
        imagePanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        
        JPanel commandPanel = new JPanel();
        commandPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        GroupLayout groupLayout = new GroupLayout(frmMultimediaGraphicalInterface.getContentPane());
        groupLayout.setHorizontalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                        .addComponent(imagePanel, GroupLayout.DEFAULT_SIZE, 575, Short.MAX_VALUE)
                        .addComponent(commandPanel, GroupLayout.DEFAULT_SIZE, 575, Short.MAX_VALUE))
                    .addGap(7))
        );
        groupLayout.setVerticalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(imagePanel, GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(commandPanel, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
                    .addGap(6))
        );
        
        imagePathField = new JTextField();
        imagePathField.setColumns(10);
        
        JButton browseButton = new JButton("Browse...");
        browseButton.setActionCommand("browseButton");
        
        JButton loadButton = new JButton("Load!");
        loadButton.setActionCommand("loadButton");
        
        JPanel coordPanel = new JPanel();
        GroupLayout gl_commandPanel = new GroupLayout(commandPanel);
        gl_commandPanel.setHorizontalGroup(
            gl_commandPanel.createParallelGroup(Alignment.TRAILING)
                .addGroup(gl_commandPanel.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(imagePathField, GroupLayout.DEFAULT_SIZE, 318, Short.MAX_VALUE)
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addGroup(gl_commandPanel.createParallelGroup(Alignment.TRAILING, false)
                        .addComponent(loadButton, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
                        .addComponent(browseButton, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addComponent(coordPanel, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                    .addGap(12))
        );
        gl_commandPanel.setVerticalGroup(
            gl_commandPanel.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_commandPanel.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(gl_commandPanel.createParallelGroup(Alignment.LEADING)
                        .addComponent(coordPanel, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                        .addGroup(gl_commandPanel.createSequentialGroup()
                            .addGroup(gl_commandPanel.createParallelGroup(Alignment.BASELINE)
                                .addComponent(imagePathField, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                                .addComponent(browseButton))
                            .addPreferredGap(ComponentPlacement.RELATED)
                            .addComponent(loadButton)))
                    .addContainerGap(17, Short.MAX_VALUE))
        );
        coordPanel.setLayout(new GridLayout(0, 2, 0, 0));
        
        JLabel xCoordLabel = new JLabel("x :");
        coordPanel.add(xCoordLabel);
        
        xValueLabel = new JLabel("0");
        coordPanel.add(xValueLabel);
        
        JLabel yCoordLabel = new JLabel("y :");
        coordPanel.add(yCoordLabel);
        
        yValueLabel = new JLabel("0");
        coordPanel.add(yValueLabel);
        commandPanel.setLayout(gl_commandPanel);
        frmMultimediaGraphicalInterface.getContentPane().setLayout(groupLayout);
        imagePanel.setLayout(new BorderLayout(0, 0));

        // Adding action listeners
        mouseListener = new MousePanelListener( xValueLabel, yValueLabel);
        buttonListener = new ActionButtonListener(imagePathField, imagePanel , mouseListener);
        browseButton.addActionListener( buttonListener );
        loadButton.addActionListener(buttonListener);
        
        
        
    }
}
