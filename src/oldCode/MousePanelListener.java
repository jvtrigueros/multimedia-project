/**
 * 
 */
package oldCode;

import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;


/**
 * @author trigueros
 *
 */
public class MousePanelListener extends MouseInputAdapter
{
    private JLabel xCoord;
    private JLabel yCoord;
    private Point originOffset;
    
    public MousePanelListener( JLabel x, JLabel y)
    {
        xCoord = x;
        yCoord = y;
        originOffset = new Point(0, 0);
    }
    
    @Override
    public void mouseDragged( MouseEvent e )
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseMoved( MouseEvent e )
    {
        // Store the ImagePanel object
        ImagePanel image = (ImagePanel) e.getComponent();
        
        // Obtain point information relative to the imagepanel
        PointerInfo a = MouseInfo.getPointerInfo();
        Point point = new Point(a.getLocation());
        SwingUtilities.convertPointFromScreen(point, image);
        
        // Only display points that are within the painted image
        
        Point realCoord = new Point(point.x - image.getImageOrigin().x, point.y - image.getImageOrigin().y );
        
        // Display only valid x,y coordinates
        if( realCoord.x < 0 )
            xCoord.setText(Integer.toString(0));
        else if( realCoord.x > image.getDisplayImage().getWidth() )
            xCoord.setText(Integer.toString(image.getDisplayImage().getWidth()));
        else
            xCoord.setText(Integer.toString(realCoord.x));
        
        if( realCoord.y < 0 )
            yCoord.setText(Integer.toString(0));
        else if( realCoord.y > image.getDisplayImage().getHeight() )
            yCoord.setText(Integer.toString(image.getDisplayImage().getWidth()));
        else
            yCoord.setText(Integer.toString(realCoord.y));
    }

    /**
     * @return the originOffset
     */
    public Point getOriginOffset()
    {
        return originOffset;
    }

    /**
     * @param originOffset the originOffset to set
     */
    public void setOriginOffset( Point originOffset )
    {
        this.originOffset = originOffset;
    }

}
