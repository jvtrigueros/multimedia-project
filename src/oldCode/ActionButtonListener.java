/**
 * 
 */
package oldCode;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

import multimedia.util.ProjectConstants;

/**
 * @author trigueros
 *
 */
public class ActionButtonListener implements ActionListener
{
    private JTextField imagePathTextField = null;
    private JPanel imagePanel = null;
    private MousePanelListener mouseListener; 
    
    public ActionButtonListener( JTextField imagePath, JPanel imagePlace , MousePanelListener mList )
    {
        imagePathTextField = imagePath;
        imagePanel = imagePlace;
        mouseListener = mList;
    }

    @Override
    public void actionPerformed( ActionEvent e )
    {
        JFileChooser browseImage = null;
        if( e.getActionCommand().equals("browseButton") )
        {
            browseImage = new JFileChooser();
            browseImage.setCurrentDirectory( new File( ProjectConstants.IMAGE_DIR )); 
            // Invoke File Chooser
            int selectedOption = browseImage.showOpenDialog((Component) e.getSource());
            // Clear the text field
            imagePathTextField.setText("");
            
            if( selectedOption == JFileChooser.APPROVE_OPTION )
            {
                // Set the path
                imagePathTextField.setText(browseImage.getSelectedFile().getAbsolutePath() );
            }
        }
        else if ( e.getActionCommand().equals("loadButton") )
        {
            // Get rid of what was there before
            if( imagePanel.getComponentCount() != 0 )
            {
                imagePanel.removeAll();
            }
            
            // Display Image selected.
            ImagePanel image = new ImagePanel(imagePathTextField.getText());
            
            if( image.getDisplayImage() != null )
            {
                imagePanel.add(image);
                imagePanel.revalidate();
                image.addMouseMotionListener(mouseListener);
                mouseListener.setOriginOffset(image.getImageOrigin());
            }
        }
    }

}
